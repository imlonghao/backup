#!/bin/bash

TODAY=$1
GDRIVEDIRECTORY=$2

if [ -z $TODAY ] || [ -z $GDRIVEDIRECTORY ]; then
    exit 1
fi

BASE=/opt/backup

source ${BASE}/env.sh

cd ${BASE}
sshpass -p ${PASSWORD} rsync -R -re "ssh -p ${PORT} -o StrictHostKeychecking=no" ${USERNAME}@${HOST}:$1 .

gdrive upload -p $2 -r $1

rm -rf $1