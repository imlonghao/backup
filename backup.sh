#!/bin/bash

BASE=/opt/backup

source ${BASE}/env.sh
source ${BASE}/hosts.sh

function _die() {
    printf "Error: $*\n"
    exit 1
}

function _pre() {
    export TODAY=$(date "+%Y%m%d")
    export HOSTNAME=$(hostname|sed "s/-//")
    export WORKINGDIRECTORY=/opt/backup/${TODAY}/${HOSTNAME}
    mkdir -p ${WORKINGDIRECTORY}
    ${HOSTNAME}
}

# Backup files
function _backup() {
    for SCRIPT in ${SCRIPTS[*]}; do
        if [ ! -e "${BASE}/scripts/${SCRIPT}.sh" ]; then
            _die "File ${BASE}/scripts/${SCRIPT}.sh Not Exist."
        fi
        /bin/bash ${BASE}/scripts/${SCRIPT}.sh
    done
}

# GPG Encrypt
function _gpg() {
    for FILE in $(ls ${WORKINGDIRECTORY}); do
        gpg --encrypt --recipient ${PUBLICKEY} --trust-model always --yes --output ${WORKINGDIRECTORY}/${FILE}.gpg ${WORKINGDIRECTORY}/${FILE}
        rm ${WORKINGDIRECTORY}/${FILE}
    done
}

# rsync
function _rsync() {
    cd /opt/backup
    sshpass -p ${PASSWORD} rsync -R -re "ssh -p ${PORT} -o StrictHostKeychecking=no" ${TODAY}/${HOSTNAME} ${USERNAME}@${HOST}:.
}

function _post() {
    rm -rf /opt/backup/${TODAY}
}

_pre
_backup
_gpg
_rsync
_post