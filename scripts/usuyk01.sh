#!/bin/bash

function _pgdump() {
    pg_dump --username $1 | gzip > ${WORKINGDIRECTORY}/$1.sql.gz
}

tar zcf ${WORKINGDIRECTORY}/thelounge.tar.gz /etc/thelounge
tar zcf ${WORKINGDIRECTORY}/opt.tar.gz --exclude /opt/backup /opt
tar zcf ${WORKINGDIRECTORY}/nspawn.tar.gz /etc/systemd/nspawn
tar zcf ${WORKINGDIRECTORY}/machines.tar.gz /var/lib/machines
tar zcf ${WORKINGDIRECTORY}/serivces.tar.gz /usr/lib/systemd/system

_pgdump bilibili
_pgdump imlonghao
_pgdump nebulas
_pgdump watcher
_pgdump xicmp