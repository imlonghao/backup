#!/bin/bash

cp /etc/v2ray/config.json ${WORKINGDIRECTORY}/v2ray.json
cp /etc/shadowsocks-libev/config.json ${WORKINGDIRECTORY}/shadowsocks.json
cp /etc/dnsmasq.conf ${WORKINGDIRECTORY}/dnsmasq.conf