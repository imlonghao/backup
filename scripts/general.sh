#!/bin/bash

tar zcf ${WORKINGDIRECTORY}/network.tar.gz /etc/systemd/network
tar zcf ${WORKINGDIRECTORY}/bird.tar.gz /etc/bird.conf /etc/bird
tar zcf ${WORKINGDIRECTORY}/ssh.tar.gz /root/.ssh
tar zcf ${WORKINGDIRECTORY}/wireguard.tar.gz /etc/wireguard
tar zcf ${WORKINGDIRECTORY}/salt.tar.gz /etc/salt
crontab -l > ${WORKINGDIRECTORY}/crontab

if [ -e "/etc/iptables/iptables.rules" ]; then
    cp /etc/iptables/iptables.rules ${WORKINGDIRECTORY}/iptables.rules
fi